const socket = io()

const chat = document.querySelector('.chat-form')
const Input = document.querySelector('.chat-input')
const Userform = document.querySelector('.user-form')
const User = document.querySelector('.user-input')

chat.addEventListener('submit', event => {
  event.preventDefault()

  socket.emit('chat', {
    from: window.sessionStorage.user ,
    to: selectedUser.name,
    message: Input.value
  })
  Input.value = ''

  socket.emit('getMessagesRequest', {
    loginUser :  window.sessionStorage.user ,
    openUser : selectedUser.name
  })
})

var isLogin =  window.sessionStorage.user 
if(isLogin)
{
  document.getElementById("loginForm").style.display = "none";
  document.getElementById("dashBoard").innerHTML =  "<h2> Login as "+ isLogin+"</h2><button  onClick='logout()'>Logout</button>"
  document.getElementById("container").style.display = "block";
  document.getElementById("userContainer").style.display = "block";

  socket.emit('registerUser', isLogin)
}
var UserList = []


var selectedUser = null


function login(){
  localStorage.removeItem('user')
  var x=document.getElementById("name").value;
  document.getElementById("loginForm").style.display = "none";
  document.getElementById("dashBoard").innerHTML =  "<h2> Login as "+ x+"</h2><button  onClick='logout()'>Logout</button>"
  document.getElementById("container").style.display = "block";
  window.sessionStorage.user = x
  socket.emit('registerUser', x)
}

function logout(){
  socket.emit('logout', isLogin)
  isLogin =''
  window.sessionStorage.user = ''
  document.getElementById("loginForm").style.display = "block";
  document.getElementById("dashBoard").innerHTML =  ""
  document.getElementById("container").style.display = "none";
}

const chatWindow = document.querySelector('.chat-window')

const renderMessage = message => {
  const div = document.createElement('div')
  div.classList.add('render-message')
  div.innerHTML = `<span><b>${message.from == window.sessionStorage.user ? 'You' : message.from }:-</b>${ message.message }</span>`
  chatWindow.appendChild(div)
}

socket.on('chat', message => {
  // make sure to modify this
  if(selectedUser.name == message.from)
      renderMessage(message)

})

socket.on('messagesList', message => {
  document.getElementById("username").innerHTML = ''

  for (let i = 0; i < message.length; i++) {
    renderMessage(message[i])
  }
})



//show the user in frontend
socket.on('sendConnectedUser', users => {

 let index = users.findIndex((s)=>s.name == window.sessionStorage.user )
  users.splice(index,1)
  UserList = users


  selectedUser = users.length ? users[0] :null
  

  let maindiv = document.getElementById('userContainer'); 
  maindiv.innerHTML = ''

  var ul = document.createElement('ul');
  ul.setAttribute('style', 'padding: 0; margin: 0;');
  ul.setAttribute('id', 'theList');

  for (i = 0; i <= UserList.length - 1; i++) {
    var li = document.createElement('li');    
    li.innerHTML = "<li  id=user"+ i +"  onClick=onUser("+ i + ")>"+ UserList[i].name +"</li>"      // assigning text to li using array value.
    ul.appendChild(li); 
  }

maindiv.appendChild(ul);  

if(selectedUser)
     onUser(0)
})


function onUser(index){
  selectedUser = UserList[index]

  for (let i = 0; i < UserList.length; i++) {
    document.getElementById('user'+i).style.color = 'black'  
  }

  document.getElementById('user'+index).style.color = 'green'  
  socket.emit('getMessagesRequest', {
    loginUser :  window.sessionStorage.user ,
    openUser : selectedUser.name
  })
}




