const express = require('express')
const app = express()
const path = require('path')
app.use(express.static(path.join(__dirname + '/public')))
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const port = process.env.PORT || 3000

var registerUser = []
var Messages = []

  io.on('connection', socket => {
    console.log('conected',socket.id)
    io.emit('sendConnectedUser', registerUser)
    socketEvent(socket)

  })



function socketEvent (socket) {
  socket.on('registerUser', payload => {
    console.log('I am User: ', payload)
    let isAlreadyExist = registerUser.findIndex((us)=>us.name == payload)

    if(isAlreadyExist > -1)
      registerUser[isAlreadyExist].sockedId = socket.id
    else  
    registerUser.push({
      name: payload,
      sockedId : socket.id
    })


     io.emit('sendConnectedUser', registerUser)
  })


  socket.on('logout', payload => {
    let isAlreadyExist = registerUser.findIndex((us)=>us.name == payload)

    if(isAlreadyExist > -1)
      registerUser.splice(isAlreadyExist,1)

     io.emit('sendConnectedUser', registerUser)
  })

  socket.on('getMessagesRequest', user => {
    let allMessages = Messages.filter((us)=>{
      if((us.from == user.openUser && us.to == user.loginUser) || (us.from == user.loginUser && us.to == user.openUser))
          return true

    })

    socket.emit('messagesList', allMessages)

  })
  

  socket.on('chat', payload => {
    console.log('From server: ', payload)

    Messages.push({
      from: payload.from,
      to: payload.to,
      message: payload.message
    })

    console.log(Messages)

     let socketObjet = registerUser.find((r)=>r.name == payload.to)

     io.to(socketObjet.sockedId).emit('chat', payload)
  })
}




server.listen(port, () => {
  console.log(`Server running on port: ${port}`)
})